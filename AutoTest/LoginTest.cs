﻿using System;
using System.Configuration;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;

using AutoTest.pages;

namespace AutoTest
{
    [TestFixture]
    public class LoginTest
    {
        private static string igWorkDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); // рабочий каталог, относительно исполняемого файла (в нашем случае относительно DLL)
        private static IWebDriver driver;
        private static ChromeOptions options;

        [OneTimeSetUp] 
        public void OneTimeSetUp()
        {
            options = new ChromeOptions();
            options.AddArguments("--ignore-certificate-errors");
            options.AddArguments("--ignore-ssl-errors");

        }

        [SetUp] 
        public void SetUp()
        {

            driver = new ChromeDriver(igWorkDir, options);
            driver.Manage().Window.Maximize();
        }

        [TearDown] 
        public void TearDown()
        {
            var myUniqueFileName = string.Format(@"{0}.png", DateTime.Now.Ticks);
            string str = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "screenshots/"+myUniqueFileName);
            ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(str);
            
            driver.Close();
            driver.Quit();
        }


        [TestCase("testolya18@gmail.com","1979492test")]
        [TestCase("testolya19@gmail.com","1979492test")]
        [TestCase("testolya18@gmail.com","1979492tes")]
        [TestCase("testolya18gmail.com","1979492test")]
        [TestCase(" testolya18@gmail.com ","1979492test")]
        [TestCase("testolya18@gmail.com"," 1979492test ")]
        [TestCase("","1979492test")]
        [TestCase("  ","1979492test")]
        [TestCase("testolya18@gmail.com","")]
        [TestCase("testolya18@gmail.com","  ")]
        
        [Test]
        public void EnterEmailTest(String login, String password)
        {
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["BaseUrl"]);
            LoginPage loginPage = new LoginPage(driver);
            WelcomePage welcomePage = loginPage.EnterEmail(login);
            InboxPage inboxPage = welcomePage.EnterPassword(password);
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(20));
            Assert.True(inboxPage.AccountIsCorrect(login));
            inboxPage.Logout();
            
            
            
        }
        
    }
}

﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AutoTest.pages
{
    public class WelcomePage : AbstractPage
    {
        [FindsBy(How = How.XPath, Using = "//input[@type='password']")]
        public IWebElement PasswordForm;

        [FindsBy(How = How.XPath, Using = "//div[@id='passwordNext']")]
        public IWebElement NextBtn;

        public WelcomePage(IWebDriver driver) : base(driver)
        {
        }

        public InboxPage EnterPassword(String password)
        {
            WaitUntil(PasswordForm);
            PasswordForm.SendKeys(password);
            WaitUntil(NextBtn);
            NextBtn.Click();
            
            return new InboxPage(driver);
        }
        
    }
}
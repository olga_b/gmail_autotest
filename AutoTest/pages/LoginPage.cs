﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Reflection;

namespace AutoTest.pages
{
    public class LoginPage : AbstractPage
    {

        [FindsBy(How = How.CssSelector, Using = "input[type*='email']")]
        public IWebElement EmailForm;

        [FindsBy(How = How.XPath, Using = "//div[@id='identifierNext']")]
        public IWebElement NextBtn;

        public LoginPage(IWebDriver driver) : base(driver)
        {
        }
        
        public WelcomePage EnterEmail(String email)
        {
            WaitUntil(EmailForm);
            
            EmailForm.SendKeys(email);
            WaitUntil(NextBtn);
            NextBtn.Click();  
            
            return new WelcomePage(driver);
        }
    }
}
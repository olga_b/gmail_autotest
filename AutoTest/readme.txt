Проект Autotest содержит в себе тестовые классы для тестирования сервиса gmail.

LoginTest.cs - тестовый класс, который содержит в себе тестовый метод EnterEmailTest(String login, String password), который выполняет тестирование формы авторизации. Параметры login, password передаются через аттрибут [TestCase]
По окончании тестов, делается скриншот, который кладется в папку screenshots для наглядного выявления причины завершения теста.

GmailTest.cs - тестовый класс, который содержит тестовые методы PrintFirstAndLastMessageFromInbox() и PrintFirstMessageFromAuthor().
PrintFirstAndLastMessageFromInbox() выводит содержимое первого и последнего письма в консоль из папки входящие на первой странице.
PrintFirstMessageFromAuthor() выводит содержимое последнего письма определенного отправителя в консоль из папки входящие на первой страницы.

Параметры UserID - логин пользователя аккаунта gmail, Query - запрос для поиска писем от определенного отправителя задаются в App.config

Credentials пользователя акккаунта gmail для доступа к его почтовому ящику лежат в файле resources/client_secret.json. При смене акканта этот файл необходимо заново перегенерировать для соответствующего пользователя через https://console.developers.google.com/flows/enableapi?apiid=gmail&hl=ru
